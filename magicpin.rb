class PasswordChecker
    def initialize(password)
        @password = password
    end

    def authentic
        if(right_length && right_characters)
            true
        else
            false
        end
    end

    def right_length
        (@password.length > 6) && (@password.length < 12)
    end

    def right_characters
        if(!@password.count("$#_=@").zero?)
            if(@password.count("%[]!()").zero?)
                @password =~ /^(?:[0-9]+[a-z]+[A-Z]|[0-9]+[A-Z]+[a-z]|[A-Z]+[0-9]+[a-z]|[A-Z]+[a-z]+[0-9]|[a-z]+[A-Z]+[0-9]|[a-z]+[0-9]+[A-Z])[a-z0-9]*$/
            end
        end
    end
end

puts "Enter comma separated passwords to be checked by the module :"

stringOfInput = gets.chomp

arrayOfInputs = stringOfInput.split(',')

arrayOfInputs.each do |password|
    passwordObject = PasswordChecker.new(password)
    if(passwordObject.authentic)
        puts "Success"
    else
        puts "Failure"
    end
end
